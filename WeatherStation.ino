/*
 * Inspired by a lot of different ideas
 * An Arduino-based weather station.
 * 
 * Owing to memory limitations, this sketch does NOT use SSL.
 * Be careful what you send over it.
 */

 /*
  * PORT ALLOCATIONS
  * PORT      DESIGNATION     DIRECTION     TYPE      DESCRIPTION
  * D13       LED_PIN         OUT           DIGITAL   On-Board LED.  General Indication
  * D12       DATA_PIN        OUT           DIGITAL   "Network" LED.  Activity indication
  * D3        RAIN_PIN        IN            DIGITAL   Rain Sensor Input - 1 pulse/tip
  * D2        SPEED_PIN       IN            DIGITAL   Speed Sensor Input - 1 pulse/revolution
  * A5        SCK             OUT           DIGITAL   I2C SCK (Clock Signal)
  * A4        SDA             IN/OUT        DIGITAL   I2C SDA (Data I/O)
  * A1        VANE_PIN        IN            ANALOGUE  Wind Direction
  * A0        UV_PIN          IN            ANALOGUE  UV Sensor
  * 
  */

/*
 *
 * Note:  You need to create the ElasticSearch Index and Mappings beforehand
 * curl -XPUT 'http://host:9200/test?pretty' -H 'Content-Type: application/json' -d'
{
    "mappings" : {
        "reading" : {
            "properties" : {
                "deviceid" : { "type" : "text" },
                "temperature" : { "type" : "float" },
                "humidity" : { "type" : "float" },
                "pressure" : { "type" : "float" },
                "dewpoint" : { "type" : "float" },
                "windspeed" : { "type" : "float" },
                "winddirection" : { "type" : "float" },
                "uv" : { "type" : "float" },
                "timestamp" : { "type" : "date" } 
            }
        }
    }
}
'

  */
#include <TimerOne.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#include <Dhcp.h>
#include <EthernetClient.h>
#include <Dns.h>
#include <BME280.h>
#include <BME280I2C.h>

//#define _DEBUG
#define _SPLUNK
//#define _ELASTIC

#ifdef _ELASTIC
#define HOST        "elastic.emergenttechnology.com.au"
#define PORT        9200
#endif
#ifdef _SPLUNK
#define HOST        "ec2-13-54-223-123.ap-southeast-2.compute.amazonaws.com"
#define PORT        8088
#endif
#define BUCKET_SIZE 0.2794   // Rain in mm per tipping event
#define ROT_VALUE   0.666667 // Distance of one rotation
#define RAIN_PIN    3
#define SPEED_PIN   2
#define UV_PIN      (A0)
#define VANE_PIN    (A1)
#define LED_PIN     13
#define DATA_PIN    12

BME280I2C bme;
EthernetClient client;
EthernetUDP udp;
unsigned long unixTime;
  

float temperature;
float humidity;
float pressure;
float windSpeed;
float dewPoint;
unsigned long tipCount;
unsigned long contactTime;
unsigned long contactBounceTime;
unsigned long totalRainfall;
unsigned long rotationCount;
unsigned int uv;
unsigned int timerCount;
unsigned int readingTime;
unsigned int totalMinutes;
int vaneReading;
int windDirection;
boolean masterStatus;

void readTemperature() {
  temperature = bme.temp(true);
}

void readHumidity() {
  humidity = bme.hum();
}

void readPressure() {
  pressure = bme.pres(1);
}

void readDewPoint() {
  dewPoint = bme.dew(bme.temp(), bme.hum(), true);
}

/*
 * Vout (mV)    UV Index
 * < 50         0
 * 227          1
 * 318          2
 * 408          3
 * 503          4
 * 606          5
 * 696          6
 * 795          7
 * 881          8
 * 976          9
 * 1079         10
 * > 1170       11+
 */
void readUV() {
//  uv = analogRead(UV_PIN);
  uv = map(analogRead(UV_PIN), 0, 1023, 0, 5000);
}

void readWindDirection() {
  vaneReading = analogRead(VANE_PIN);
  windDirection = map(vaneReading, 0, 1023, 0, 5000); // Convert to mV
}

void isr_rain() {
  if((millis() - contactTime) > 15) {
    tipCount++;
    totalRainfall = tipCount * BUCKET_SIZE;
    contactTime = millis();
  }
}

void isr_wind() {
  // Temporary.  Show that we are in the ISR
  digitalWrite(DATA_PIN, HIGH);
  if((millis() - contactBounceTime) > 15) {
    rotationCount ++;
    contactBounceTime = millis();
  }
  digitalWrite(DATA_PIN, LOW);
}

void isr_timer() {
  digitalWrite(LED_PIN, HIGH);
  
  timerCount++;
  readingTime++;

  if(timerCount == 5) {
    // Need to divide count by 60 to get m/s
    // Then multiply by 3.6 for Km/h
    windSpeed = rotationCount / 60 * ROT_VALUE * 3.6;
    rotationCount = 0;
    timerCount = 0;
  }
  digitalWrite(LED_PIN, LOW);
}

void blinkLED(unsigned int times) {
  for(int i = 0; i < times; i++) {
    digitalWrite(LED_PIN, HIGH);
    delay(75);
    digitalWrite(LED_PIN, LOW);
    delay(75);
  }
}

unsigned long inline ntpUnixTime (UDP &udp)
{
  static int udpInited = udp.begin(123); // open socket on arbitrary port

  const char timeServer[] = "pool.ntp.org";  // NTP server

  // Only the first four bytes of an outgoing NTP packet need to be set
  // appropriately, the rest can be whatever.
  const long ntpFirstFourBytes = 0xEC0600E3; // NTP request header

  // Fail if WiFiUdp.begin() could not init a socket
  if (! udpInited)
    return 0;

  // Clear received data from possible stray received packets
  udp.flush();

  // Send an NTP request
  digitalWrite(DATA_PIN, HIGH);
  if (! (udp.beginPacket(timeServer, 123) // 123 is the NTP port
   && udp.write((byte *)&ntpFirstFourBytes, 48) == 48
   && udp.endPacket()))
    return 0;       // sending request failed

#ifdef _DEBUG
  Serial.println("Waiting for ntp");
#endif
  // Wait for response; check every pollIntv ms up to maxPoll times
  const int pollIntv = 150;   // poll every this many ms
  const byte maxPoll = 15;    // poll up to this many times
  int pktLen;       // received packet length
  for (byte i=0; i<maxPoll; i++) {
    if ((pktLen = udp.parsePacket()) == 48)
      break;
    delay(pollIntv);
  }
  if (pktLen != 48)
    return 0;       // no correct packet received

  // Read and discard the first useless bytes
  // Set useless to 32 for speed; set to 40 for accuracy.
  const byte useless = 40;
  for (byte i = 0; i < useless; ++i)
    udp.read();

  // Read the integer part of sending time
  unsigned long time = udp.read();  // NTP time
  for (byte i = 1; i < 4; i++)
    time = time << 8 | udp.read();

  // Round to the nearest second if we want accuracy
  // The fractionary part is the next byte divided by 256: if it is
  // greater than 500ms we round to the next second; we also account
  // for an assumed network delay of 50ms, and (0.5-0.05)*256=115;
  // additionally, we account for how much we delayed reading the packet
  // since its arrival, which we assume on average to be pollIntv/2.
  time += (udp.read() > 115 - pollIntv/8);

  // Discard the rest of the packet
  udp.flush();
  digitalWrite(DATA_PIN, LOW);
  return time - 2208988800ul;   // convert NTP time to Unix time
}



void setup() {
  pinMode(LED_PIN, OUTPUT);
  pinMode(DATA_PIN, OUTPUT);
  pinMode(RAIN_PIN, INPUT);
  pinMode(SPEED_PIN, INPUT);
  digitalWrite(DATA_PIN, HIGH);
  
  // If using the Freetronics EtherTen board,
  // the W5100 takes a bit longer to come out of reset
  delay(100);   // This should be plenty

#ifdef _DEBUG
  Serial.begin(9600);
  while(!Serial) {
    blinkLED(1);
    delay(10);    // Wait for serial port
  }
#endif
  
  while(bme.begin() == false) {
#ifdef _DEBUG    
    Serial.println("Waiting for BME280");
#endif
    blinkLED(2);
    delay(1000);
  }

#ifdef _DEBUG
  Serial.println("BME280 is initialised!");
#endif

  byte mac[] = {
    0x02, 0x60, 0x8C, 0x01, 0x02, 0x03
  };
  while(Ethernet.begin(mac) == 0) {
#ifdef _DEBUG    
    Serial.println("Waiting for DHCP");
#endif
    blinkLED(3);
    delay(1000);
  }
#ifdef _DEBUG
  Serial.println("Network is active");
#endif

  // Set all the globals to a valid starting point.
  temperature = 0.0;
  humidity = 0.0;
  pressure = 0.0;
  uv = 0;
  totalRainfall = 0;
  tipCount = 0;
  contactTime = 0;
  contactBounceTime = 0;
  rotationCount = 0;
  timerCount = 0;
  readingTime = 0;
  totalMinutes = 0;   // Use this to determine 24 hours

  // Initialise the interrupt for the rain gauge
  attachInterrupt(digitalPinToInterrupt(RAIN_PIN), isr_rain, FALLING);
#ifdef _DEBUG
  Serial.println("Attached Rain Sensor");
#endif

  // Initialise the interrupt for wind speed
  attachInterrupt(digitalPinToInterrupt(SPEED_PIN), isr_wind, RISING);
#ifdef _DEBUG
  Serial.println("Attached Speed Sensor");
#endif

  // Set up a timer for 1/2 second intervals
  Timer1.initialize(500000);
  Timer1.attachInterrupt(isr_timer); 
#ifdef _DEBUG
  Serial.println("Attached Timer");
#endif

  sei();    // Enable interrupts
  
  digitalWrite(DATA_PIN, LOW);
}

void loop() {
  if(readingTime > 119) {    // 1 minute has expired

    blinkLED(4);
    readTemperature();
    readHumidity();
    readPressure();
    readDewPoint();
    readUV();

    digitalWrite(DATA_PIN, HIGH);
    // Let's build a stamp for the elastic store
    unixTime = ntpUnixTime(udp);

#ifdef _ELASTIC
    String data = "{ \"deviceid\": \"02608C010203\",";
    data += "\"temperature\": \"" + String(temperature) + "\", ";
    data += "\"humidity\": \"" + String(humidity) + "\", ";
    data += "\"pressure\": \"" + String(pressure) + "\", ";
    data += "\"dewpoint\": \"" + String(dewPoint) + "\", ";
    data += "\"rainfall\": \"" + String(totalRainfall) + "\", ";
    data += "\"windspeed\": \"" + String(windSpeed) + "\", ";
    data += "\"winddirection\" :\"" + String(windDirection) + "\", ";
    data += "\"uv\": \"" + String(uv) + "\", ";
    data += "\"timestamp\" :\"" + String(unixTime) + "\"";
    data += " }";
#endif

#ifdef _SPLUNK
    String data = "{ \"host\": \"02608C010203\",";
    data += "\"sourcetype\": \"arduino\", ";
    data += "\"index\": \"myiot\", ";
    data += "\"event\": { ";
    data += "\"temp\": \"" + String(temperature) + "\", ";
    data += "\"hum\": \"" + String(humidity) + "\", ";
    data += "\"press\": \"" + String(pressure) + "\", ";
    data += "\"dewpt\": \"" + String(dewPoint) + "\", ";
    data += "\"rain\": \"" + String(totalRainfall) + "\", ";
    data += "\"speed\": \"" + String(windSpeed) + "\", ";
    data += "\"dir\": \"" + String(windDirection) + "\", ";
    data += "\"uv\": \"" + String(uv) + "\" ";
    data += " }}";
#endif

#ifdef _DEBUG
    Serial.println("Connecting to: " + String(HOST) + ":" + String(PORT));
#endif    
    if(client.connect(HOST, PORT)) {
#ifdef _DEBUG
      Serial.println("Connected");
      Serial.println(data);
#endif
#ifdef _ELASTIC
      String url = "/test/reading";
#endif
#ifdef _SPLUNK
      String url = "/services/collector";
#endif
      client.println("POST " + url + " HTTP/1.1");
#ifdef _SPLUNK
      client.println("Authorization: Splunk BC9997B9-8381-4D82-A44E-8D05357018D3");
#endif
      client.print("Content-Length: ");
      client.println(data.length());
      client.println();
      client.println(data);
      client.println();
      
      delay(50);
      
      if(client.connected()) {
        client.flush();
        client.stop();
      }
      digitalWrite(DATA_PIN, LOW);
    }
    else {
#ifdef _DEBUG
    Serial.println("Connect failed");
#endif      
    }
    readingTime = 0;
    // General housekeeping stuff
    totalMinutes++;
    if(totalMinutes > 1440) { // 24 hours
      totalRainfall = 0;
      totalMinutes = 0;
    }
  }
}


